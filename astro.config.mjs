import { defineConfig } from 'astro/config';
import react from "@astrojs/react";

import compress from "astro-compress";

// https://astro.build/config
export default defineConfig({
  integrations: [react(), compress()],
  // GitLab Pages requires exposed files to be located in a folder called "static".
  // So we're instructing Astro to put the static build output in a folder of that name.
  outDir: 'public',

  // The folder name Astro uses for static files (`static`) is already reserved
  // for the build output. So in deviation from the defaults we're using a folder
  // called `static` instead.
  publicDir: 'static',
});
