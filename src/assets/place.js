initYMap();

function initYMap() {
    if (!'IntersectionObserver' in window) {
        return;
    }

    const observerElement = document.querySelectorAll('[data-yamap]');
    const observerOptions = {
        root: null,
        rootMargin: '0px -100px',
        threshold: 0,
    };
    const observer = new IntersectionObserver(observerCallback, observerOptions);

    [].forEach.call(observerElement, (yamap) => observer.observe(yamap));

    function observerCallback(entries) {
        entries.forEach((entry) => {
            const yamapEl = entry.target;

            if (entry.intersectionRatio > 0) {
                const script = document.createElement("script");
                // https://yandex.ru/dev/maps/jsapi/doc/2.1/quick-start/index.html#get-api-key
                script.src = "https://api-maps.yandex.ru/2.1/?apikey=81cbe414-ec11-41dd-9139-ab906a063306&lang=ru_RU";

                document.head.appendChild(script);
                script.onload = () => {
                    ymaps.ready(init)
                };
                observer.unobserve(entry.target);
            }
        });
    }

    function init() {
        var myMap = new ymaps.Map("map", {
            center: [56.835970230937434, 60.614526156942084],
            zoom: 15,
            controls: [],
        }, {suppressMapOpenBlock: true});

        var myPlacemark = new ymaps.Placemark([56.83646422457333, 60.61429012254882], {
            // Свойства метки
            hintContent: 'Екатеринбург, High Heels',
            balloonContent: 'Valentina'
        });

        myMap.geoObjects.add(myPlacemark);
    }
}
