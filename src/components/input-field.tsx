import type {JSX} from "astro/jsx-runtime";
import './form/form.css';
import React from "react";

type inputTextProps = {
    className?: string;
    type: string;
    text: string;
    placeholder: string;
    name: string;
    id: string;
    forwardRef: React.MutableRefObject<HTMLInputElement | null>;
}

export function InputField({
                               className = "input-field",
                               type,
                               text,
                               placeholder,
                               name,
                               id,
                               forwardRef
                           }: inputTextProps): JSX.Element {
    return (
        <>
            <input
                className={className}
                placeholder={placeholder}
                type={type}
                name={name}
                id={id}
                ref={forwardRef}
                required
            />

            <label
                className="visually-hidden"
                htmlFor={id}
            >
                {text}
            </label>
        </>
    );
}
