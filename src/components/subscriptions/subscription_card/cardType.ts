export type CardType = {
    id: number,
    title: string,
    count: number,
    price: number,
    info: string,
    hasModal: boolean
};
