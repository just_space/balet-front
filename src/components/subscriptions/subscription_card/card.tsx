import type {JSX} from "astro/jsx-runtime";
import './card.css';
import type {CardType} from './cardType.ts';
import React, {useState} from "react";

type CardProps = {
    card: CardType,
    currentId: number,
    setId: (id: number) => void
}

export function Card({card, currentId, setId}: CardProps): JSX.Element {
    const [isInfoOpen, setIsInfoOpen] = useState(false);
    const [isInfoOpenByHover, setIsInfoOpenByHover] = useState(false);

    const clickInfo = () => {
        setIsInfoOpen(!isInfoOpen);
        if (isInfoOpen) {
            setId(card.id)
        }
    };

    const onMouseEnter = () => {
        setIsInfoOpenByHover(true)
        setId(card.id)
    }
    return (
        <div className="card">
            <div className="card__content">
                <div className="card__name">{card.title}
                    {card.hasModal && <div className="card__info">
                        <img onClick={clickInfo} onMouseEnter={onMouseEnter}
                             onMouseLeave={() => setIsInfoOpenByHover(false)} className="card__name__info"
                             src="./info.svg" alt="Info Icon"/>
                        {((isInfoOpen && (currentId == card.id)) || isInfoOpenByHover) &&
                            <div className="card__name__info-modal">
                                {card.info}
                            </div>}
                    </div>}
                </div>
                <svg width="101" height="1" viewBox="0 0 101 1" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <line y1="0.490234" x2="101.005" y2="0.490234" stroke="#EFEDE2" strokeDasharray="10 10"/>
                </svg>
                <div className="card__description">
                    <text>{card.count + " занятий"}</text>
                    <text>{card.price + " ₽"}</text>
                </div>
            </div>
        </div>
    )
}

export default Card;
