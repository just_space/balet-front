import type {JSX} from "astro/jsx-runtime";
import './subscriptions.css';
import Card from "./subscription_card/card.tsx";
import type {CardType} from "./subscription_card/cardType.ts";
import {useState} from "react";

export function Subscriptions(): JSX.Element {
    const [currentId, setCurrentId] = useState(0);
    const cards: CardType[] = [
        {
            id: 0,
            title: "В группе",
            price: 4800,
            count: 8,
            info: "Занятия  в группе от 5 до\n" +
                "15 человек,  где создается\n" +
                "кайфовая атмосфера \n" +
                "и общение среди девчонок",
            hasModal: true
        },
        {
            id: 1,
            title: "Индив",
            price: 2500,
            count: 1,
            info: "Беру тебя под свое крыло.\n" +
                "Будем вместе погружаться\n" +
                "в танець и изучать базовые\n" +
                "движения",
            hasModal: true
        },
        {
            id: 2,
            title: "Индив",
            price: 4800,
            count: 8800,
            info: "Беру тебя под свое крыло.\n" +
                "Будем вместе погружаться\n" +
                "в танець и изучать базовые\n" +
                "движения",
            hasModal: false
        }
    ]

    return (
        <div className="subscriptions">
            <h2 className="title_left">Абонементы</h2>
            <div className="cards">
                {cards.map(card => <Card card={card} key={card.id} currentId={currentId} setId={setCurrentId}/>)}
            </div>
        </div>
    );
}

export default Subscriptions;
