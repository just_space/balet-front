import './video-carousel.css';
import type {JSX} from "astro/jsx-runtime";
import React, {useEffect, useRef, useState} from "react";

export function VideoCarousel(): JSX.Element {
    const [mainIndex, setMainIndex] = useState(0);
    const videos = ["./dance1.mp4", "./dance2.mp4", "./dance3.mp4"]
    const handleChangeMainIndex = (index: number) => () => {
        setMainIndex(index);
    };

    const videoRef = useRef<HTMLVideoElement | null>(null);
    const callbackFunction: IntersectionObserverCallback = (entries) => {
        const [entry] = entries;
        if (videoRef.current) {
            if (!entry.isIntersecting) {
                videoRef.current.pause();
                return
            }
            videoRef.current.play();
        }
    }
    const options = {
        root: null,
        rootMargin: '0px -100px',
        threshold: 0
    }
    useEffect(() => {
        const observer = new IntersectionObserver(callbackFunction, options);
        if (videoRef.current) observer.observe(videoRef.current)
        return () => {
            if (videoRef.current) observer.unobserve(videoRef.current)
        }
    }, [videoRef, options]);

    return (
        <div className="carousel">
            <video
                ref={videoRef}
                className="carousel__video"
                autoPlay
                loop
                playsInline
                muted
                poster="./video-preview.png"
                src={videos[mainIndex]}
                typeof='video/mp4'
            />

            <div className="carousel__checkbox-wrapper">
                {videos.map((video, index) => <label key={index}>
                    <input type="radio" className="modern-radio" name="subscription-type"
                           value={video}
                           onChange={handleChangeMainIndex(index)}
                           checked={mainIndex === index} id={video}
                    />
                    <span></span>
                </label>)
                }
            </div>
        </div>
    )
}

export default VideoCarousel;
