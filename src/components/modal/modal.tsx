import type {JSX} from "astro/jsx-runtime";
import './modal.css';

type ModalProps = {
    handleCloseModal: () => void;
}

export function Modal({handleCloseModal}: ModalProps): JSX.Element {

    return (
        <div className="modal">
            <div className="modal__content">
                <button className="modal__close-btn" onClick={handleCloseModal}>
                    {" "}
                    &#10006;{" "}
                </button>
                Заявка успешно отправлена
            </div>
        </div>)
}

export default Modal;