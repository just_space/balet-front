import type {JSX} from "astro/jsx-runtime";
import './form.css';
import React, {type FormEvent, useRef, useState} from "react";
import {InputField} from "../input-field.tsx";
import Modal from "../modal/modal.tsx";

type FormData = {
    name: string,
    subscriptionType: string,
    experience?: string,
    phone?: string,
    mail?: string,
    telegram?: string
}

export function Form(): JSX.Element {
    const nameRef = useRef<HTMLInputElement | null>(null);
    const mailRef = useRef<HTMLInputElement | null>(null);
    const phoneRef = useRef<HTMLInputElement | null>(null);
    const telegramRef = useRef<HTMLInputElement | null>(null);
    const formRef = useRef<HTMLFormElement | null>(null);
    const [contacsSelect, setContacsSelect] = useState({
        mail: {
            type: "email",
            text: "Почта",
            placeholder: "почта",
            name: "mail",
            id: "mail",
            ref: mailRef
        },
        phone: {
            type: "tel",
            text: "Телефон",
            placeholder: "телефон",
            name: "phone",
            id: "phone",
            ref: phoneRef
        },
        telegram: {
            type: "text",
            text: "Телеграм",
            placeholder: "телеграм",
            name: "telegram",
            id: "telegram",
            ref: telegramRef
        },
        selectedOption: {
            type: "mail",
            text: "Почта",
            placeholder: "почта",
            name: "mail",
            id: "mail",
            ref: mailRef
        }
    });
    const [subscriptionTypeData, setSubscriptionTypeData] = useState({
        value1: 'индивидуальный',
        value2: 'в группе',
        selectedRadioInput: ''
    });
    const [experienceData, setExperienceData] = useState({
        value1: 'начики',
        value2: 'pro',
        selectedRadioInput: ''
    });
    const [needShowModal, setNeedShowModal] = useState(false);

    const handleSubmit = (evt: FormEvent<HTMLFormElement>) => {
        evt.preventDefault();
        let data: FormData = {
            name: nameRef.current?.value || '',
            subscriptionType: subscriptionTypeData.selectedRadioInput,
        }
        if (data.subscriptionType === subscriptionTypeData.value2) {
            data = {
                ...data,
                experience: experienceData.selectedRadioInput
            }
        }
        setNeedShowModal(true);
    }

    const handleSubscriptionTypeChange = (e: { target: { value: string; }; }) => {
        setSubscriptionTypeData({...subscriptionTypeData, selectedRadioInput: e.target.value});
    }

    const handleExperienceChange = (e: { target: { value: string; }; }) => {
        setExperienceData({...experienceData, selectedRadioInput: e.target.value});
    }
    const handleContacsSelectChange = (e: { target: { value: string; }; }) => {
        const value = contacsSelect[(e.target.value) as ("mail" | "telegram" | "phone")];
        setContacsSelect({...contacsSelect, selectedOption: value})
    }
    const handleCloseModal = () => {
        setNeedShowModal(false);
        if (formRef.current) {
            formRef.current.reset();
        }
    }
    return (

        <form
            className="form"
            onSubmit={handleSubmit}
            ref={formRef}
        >
            {needShowModal ? <Modal handleCloseModal={handleCloseModal}/>
                : ''}
            <div className="title"> Оставь заявку</div>
            <div className="inputs">
                <InputField {...{
                    type: "text",
                    text: "Имя",
                    placeholder: "имя",
                    name: "name",
                    id: "name",
                    forwardRef: nameRef
                }}/>
                <div className="contacts__container">
                    <select className="select__contacts" onChange={handleContacsSelectChange}>
                        {[{name: 'почта', value: 'mail'}, {name: 'телефон', value: 'phone'}, {
                            name: 'телеграм',
                            value: 'telegram'
                        }].map(
                            (item) => <option className="contacts__item" key={item.name} id={item.value}
                                              value={item.value}>{item.name}</option>
                        )}
                    </select>
                    <div className="contacts__text-hider"/>

                    <InputField {...{
                        className: "input-field input__contacs",
                        type: contacsSelect.selectedOption.type,
                        text: contacsSelect.selectedOption.text,
                        placeholder: contacsSelect.selectedOption.placeholder,
                        name: contacsSelect.selectedOption.name,
                        id: contacsSelect.selectedOption.id,
                        forwardRef: contacsSelect.selectedOption.ref,
                    }}/>

                </div>
                <div className="checkbox-wrapper">
                    <label>
                        {subscriptionTypeData.value1}
                        <input type="radio" className="modern-radio" name="subscription-type"
                               value={subscriptionTypeData.value1}
                               onChange={handleSubscriptionTypeChange}
                        />
                        <span></span>
                    </label>
                    <label>
                        {subscriptionTypeData.value2}
                        <input type="radio" className="modern-radio" value={subscriptionTypeData.value2}
                               name="subscription-type"
                               onChange={handleSubscriptionTypeChange}/>
                        <span></span>
                    </label>
                </div>

                {subscriptionTypeData.selectedRadioInput === subscriptionTypeData.value2 ?
                    <div className="checkbox-wrapper">
                        <label>
                            {experienceData.value1}
                            <input type="radio" className="modern-radio" value={experienceData.value1} name="experience"
                                   onChange={handleExperienceChange}/>
                            <span></span>
                        </label>
                        <label>
                            {experienceData.value2}
                            <input type="radio" className="modern-radio" value={experienceData.value2} name="experience"
                                   onChange={handleExperienceChange}/>
                            <span></span>
                        </label>
                    </div> : ''}

            </div>

            <button className="button" type="submit"> Отправить</button>
            <div className="button__description">

                <div className="description__arrow">
                    <svg width="38" height="17" viewBox="0 0 38 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M1.49949 0.977499C1.48707 0.701637 1.25336 0.48808 0.977499 0.500507C0.701637 0.512933 0.48808 0.746638 0.500507 1.0225L1.49949 0.977499ZM9.18186 10.0219L9.34727 9.55001L9.18186 10.0219ZM37.3519 13.3552C37.5481 13.1609 37.5496 12.8443 37.3552 12.6481L34.1884 9.45106C33.9941 9.25487 33.6775 9.25337 33.4813 9.4477C33.2851 9.64203 33.2836 9.95861 33.478 10.1548L36.2929 12.9966L33.4511 15.8116C33.2549 16.0059 33.2534 16.3225 33.4477 16.5187C33.642 16.7149 33.9586 16.7164 34.1548 16.522L37.3519 13.3552ZM1 1C0.500507 1.0225 0.500521 1.02283 0.500537 1.02318C0.500544 1.02333 0.500562 1.0237 0.500576 1.024C0.500604 1.0246 0.500638 1.02529 0.500677 1.02608C0.500756 1.02765 0.500858 1.02961 0.500986 1.03194C0.501242 1.03661 0.501604 1.04279 0.502101 1.05043C0.503094 1.06573 0.504627 1.08691 0.506926 1.11368C0.511525 1.16722 0.519197 1.24316 0.531778 1.33908C0.55693 1.53086 0.60176 1.80296 0.681042 2.13599C0.839522 2.80168 1.13637 3.71372 1.691 4.71466C2.80558 6.72614 4.94528 9.06645 9.01644 10.4937L9.34727 9.55001C5.51822 8.20764 3.567 6.03703 2.5657 4.22998C2.06239 3.32167 1.79506 2.49754 1.65385 1.90439C1.58329 1.60799 1.5444 1.37 1.52329 1.20904C1.51274 1.12859 1.50664 1.0675 1.50326 1.0281C1.50157 1.0084 1.50055 0.994142 1.5 0.985607C1.49972 0.98134 1.49956 0.978506 1.49948 0.977142C1.49945 0.97646 1.49943 0.976146 1.49943 0.976204C1.49943 0.976233 1.49944 0.976355 1.49945 0.976571C1.49946 0.976678 1.49947 0.976911 1.49947 0.976964C1.49948 0.97722 1.49949 0.977499 1 1ZM9.01644 10.4937C13.205 11.9621 20.2258 12.7083 26.1446 13.0918C29.1154 13.2842 31.8276 13.3863 33.797 13.4402C34.7818 13.4672 35.5813 13.4821 36.1348 13.4903C36.4116 13.4944 36.6269 13.4968 36.7732 13.4982C36.8464 13.4989 36.9023 13.4994 36.94 13.4996C36.9589 13.4998 36.9732 13.4999 36.9828 13.4999C36.9876 13.4999 36.9913 13.5 36.9938 13.5C36.995 13.5 36.9959 13.5 36.9966 13.5C36.9969 13.5 36.9972 13.5 36.9973 13.5C36.9975 13.5 36.9976 13.5 37 13C37.0024 12.5 37.0023 12.5 37.0022 12.5C37.0021 12.5 37.0019 12.5 37.0016 12.5C37.0011 12.5 37.0002 12.5 36.9991 12.5C36.9968 12.5 36.9933 12.5 36.9887 12.4999C36.9795 12.4999 36.9656 12.4998 36.9471 12.4997C36.9102 12.4994 36.8551 12.499 36.7828 12.4983C36.6381 12.4969 36.4246 12.4945 36.1496 12.4904C35.5998 12.4823 34.8046 12.4674 33.8244 12.4406C31.8637 12.3869 29.1644 12.2853 26.2093 12.0939C20.2765 11.7095 13.3882 10.9666 9.34727 9.55001L9.01644 10.4937Z"
                            fill="#EFEDE2"/>
                    </svg>
                </div>
                <span className="description__text">
                    Я свяжусь с тобой
                </span>
            </div>


        </form>
    );
}

export default Form;